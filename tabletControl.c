#include "tabletControl.h"

/* Function Prototypes */

void idle_action();
void check_position();
void rotate_speed(int speed_x, int speed_y); /* speed: 0-1023 CW & 1024-2047 CCW */

/* Global vars */

int currentAlpha=0, currentBeta=0;

int ref_x=320, ref_y=240;

int moving_disabled_x = 0;
int moving_disabled_y = 0;

/* Interrupts */

ISR(TIMER1_OVF_vect)
{
	check_position();
	rotate_speed(currentAlpha, currentBeta);
}

/* Main code */

#define DEFAULT_SPEED 30
#define THRESHOLD 25

int abs(int a)
{
	if (a>=0)
	return a;
	else
	return -a;
}

int main(void)
{
	// enable timer overflow interrupt for Timer1
	// every ~32 ms
	TIMSK1 = 0x01;
	TCNT1 ^= TCNT1; // zeroing the registers
	TCCR1A = 0x00;
	TCCR1B |= 0x02;
	
	// zeroing other registers
	PORTC = 0x00;
	
	DDRA  = 0xFF;
	DDRA  = 0xFC;
	PORTA = 0xFC;

	serial_initialize(57600);
	dxl_initialize( 0, DEFAULT_BAUDNUM ); // Not using device index
	sei();	// Interrupt Enable

	dxl_hal_clear();
	
	printf("Press any button in order to start\n");
	getchar();
	printf("\n\nProgram has been started\n");

	PORTC = !PORTC;
	
	char cmd;
	unsigned char arm_cmd;
	int x, y, tmp;
	
	while(1) {

		cmd = getchar();
		
		if ( cmd == 'X'){
			// obtain 3 bytes and form 3 digit number
			x = 0;
			tmp = getchar()-'0';
			x += 100*tmp;
			tmp = getchar()-'0';
			x += 10*tmp;
			tmp = getchar()-'0';
			x += tmp;
			
			// obtain 3 bytes and form 3 digit number
			y = 0;
			tmp = getchar()-'0';
			y += 100*tmp;
			tmp = getchar()-'0';
			y += 10*tmp;
			tmp = getchar()-'0';
			y += tmp;
			
			//update angles
			currentAlpha = (ref_x-x) / 640.0 *120.0;
			if(currentAlpha < 0) {
				currentAlpha = 1024 - currentAlpha;
			}
			
			currentBeta = (ref_y-y) / 480.0 * 120.0;
			if(currentBeta < 0) {
				currentBeta = 1024 - currentBeta;
			}

			check_position();
			
			printf("alpha - %d beta - %d\n", currentAlpha, currentBeta);
			printf("x move? %d, y move? %d\n",moving_disabled_x,moving_disabled_y);
			_delay_ms(5);
		} 
		
		arm_cmd = 'a';
		dxl_hal_tx(&arm_cmd, 1);
		
		//else {
		//if(cmd == 'a') {
		//arm_cmd = cmd;
		//dxl_hal_tx(&arm_cmd, 1);		
		//}
		//
		//if(cmd == 'b') {
		//arm_cmd = cmd;
		//dxl_hal_tx(&arm_cmd, 1);
		//}
		//
		//if(cmd == 'c') {
		//arm_cmd = cmd;
		//dxl_hal_tx(&arm_cmd, 1);
		//}
		//}
		
		PORTC = !PORTC;
	}
	return 0;
}

#define WANDER_SPEED 150

void idle_action()
{
	int rightAngle = 180-50;
	int leftAngle = 180+50;

	int newAngleOne;
	int newAngleTwo;
	
	if (getAngle(1) < 180) {
		newAngleOne = rightAngle;
		newAngleTwo = leftAngle;
		} else {
		newAngleOne = leftAngle;
		newAngleTwo = rightAngle;
	}
	//	cnt = 1;
	
	rotateAngle(newAngleOne, 180, WANDER_SPEED);
	Delay(2);
	rotateAngle(newAngleTwo, 180, WANDER_SPEED);
	Delay(2.5);
	
	rotateAngle(180, 180, WANDER_SPEED);
	Delay(5);
	
}

void check_position()
{
	int xPresentPos;
	int yPresentPos;
	
	xPresentPos = dxl_read_word( 1, P_PRESENT_POS_L);
	
	printf("pres pos %d, %d, %d\n", xPresentPos, ALPHA_UPPER_LIM, ALPHA_DOWN_LIM);
	
	if( xPresentPos > ALPHA_UPPER_LIM || xPresentPos < ALPHA_DOWN_LIM) {
		moving_disabled_x = 1;
		} else {
		moving_disabled_x = 0;
	}
	
	yPresentPos = dxl_read_word( 2, P_PRESENT_POS_L );
	
	printf("pres pos %d, %d, %d\n", yPresentPos, BETA_UPPER_LIM, BETA_DOWN_LIM);
	
	if( yPresentPos > BETA_UPPER_LIM || yPresentPos < BETA_DOWN_LIM) {
		moving_disabled_y = 1;
		} else {
		moving_disabled_y = 0;
	}
}

void rotate_speed(int speed_x, int speed_y)
{
	if(moving_disabled_x == 1) {
		if (speed_x < 1024)
		speed_x = 0;
		else
		speed_x = 1024;
	}
	
	if(moving_disabled_y == 1) {
		if (speed_y < 1024)
		speed_y = 0;
		else
		speed_y = 1024;
	}
	
	//dxl_write_word( BROADCAST_ID, P_MOVING_SPEED_L, speed);
	dxl_write_word( BROADCAST_ID, P_TORQUE_LIMIT_L, 500);

	dxl_set_txpacket_id(BROADCAST_ID);
	dxl_set_txpacket_instruction(INST_SYNC_WRITE);
	dxl_set_txpacket_parameter(0, P_MOVING_SPEED_L);
	dxl_set_txpacket_parameter(1, 2);

	dxl_set_txpacket_parameter(2, 1);
	dxl_set_txpacket_parameter(3, dxl_get_lowbyte(speed_x));
	dxl_set_txpacket_parameter(4, dxl_get_highbyte(speed_x));
	dxl_set_txpacket_parameter(5, 2);
	dxl_set_txpacket_parameter(6, dxl_get_lowbyte(speed_y));
	dxl_set_txpacket_parameter(7, dxl_get_highbyte(speed_y));

	dxl_set_txpacket_length((2+1)*NUM_ACTUATOR+4);
	dxl_txrx_packet();

	CommStatus = dxl_get_result();
	if( CommStatus == COMM_RXSUCCESS )
	PrintErrorCode();
	else
	PrintCommStatus(CommStatus);
}
