#define F_CPU 16000000L

#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "serial.h"
#include "dynamixel.h"
#include "dxl_hal.h"

#define CW_ANGLE_LIMIT_L	6
#define CW_ANGLE_LIMIT_H	7
#define CCW_ANGLE_LIMIT_L	8	
#define CCW_ANGLE_LIMIT_H	9
#define P_GOAL_POSITION_L	30
#define P_GOAL_POSITION_H	31
#define P_MOVING_SPEED_L	32
#define P_MOVING_SPEED_H	33
#define P_TORQUE_LIMIT_L	34
#define P_TORQUE_LIMIT_H	35
#define P_PRESENT_POS_L  	36
#define P_PRESENT_POS_H  	37
#define P_MOVING			46
#define MIDDLE				2046
#define degToValue(x)		(int) ((x)*11.375)
#define valueToDeg(x)		(int) ((x)*360.0/4096.0)
#define PI	3.141592f

// Default setting
#define DEFAULT_BAUDNUM		1 		// 1Mbps
#define NUM_ACTUATOR		2 		// Number of actuator
#define CONTROL_PERIOD		(10) 	// msec (Large value is more slow) 

#define ALPHA_UPPER_LIM		3000
#define ALPHA_DOWN_LIM		1000
#define BETA_UPPER_LIM		3150
#define BETA_DOWN_LIM		1450

int CommStatus;
int i, j, l;

void rotateAngle(int, int, int);
void Delay(int);
void waitForIDs(void);
void PrintCommStatus(int);
void PrintErrorCode(void);

void rotateAngle(int a, int b, int speed)
{		
	
	dxl_write_word( BROADCAST_ID, P_MOVING_SPEED_L, speed);
	dxl_write_word( BROADCAST_ID, P_TORQUE_LIMIT_L, 1000);
	
	dxl_set_txpacket_id(BROADCAST_ID);
	dxl_set_txpacket_instruction(INST_SYNC_WRITE);
	dxl_set_txpacket_parameter(0, P_GOAL_POSITION_L);
	dxl_set_txpacket_parameter(1, 2);
		
	dxl_set_txpacket_parameter(2, 1);
	dxl_set_txpacket_parameter(3, dxl_get_lowbyte(degToValue(a)));
	dxl_set_txpacket_parameter(4, dxl_get_highbyte(degToValue(a)));
	dxl_set_txpacket_parameter(5, 2);
	dxl_set_txpacket_parameter(6, dxl_get_lowbyte(degToValue(b)));
	dxl_set_txpacket_parameter(7, dxl_get_highbyte(degToValue(b)));
		
	dxl_set_txpacket_length(12);
	dxl_txrx_packet();
			
	CommStatus = dxl_get_result();
		if( CommStatus == COMM_RXSUCCESS )
	PrintErrorCode();
	else
		PrintCommStatus(CommStatus);

	printf("\nRotated to %d and %d\n", degToValue(a), degToValue(b));
  	//waitForIDs();	
}

inline int getAngle(int id)
{
	int wPresentPos;
	wPresentPos = dxl_read_word( id, P_PRESENT_POS_L );
				
	CommStatus = dxl_get_result();
	if( CommStatus == COMM_RXSUCCESS ){
		PrintErrorCode();
		return -1;
	}
	else
		PrintCommStatus(CommStatus);
	return wPresentPos;	
}


void Delay(int k)
{
/*
	delay in s if k>1
	delay in from 0 to 1000 ms if k<1
*/
	while(k>0)
	{
		if(k>=1)	_delay_ms(1000);
		else if(k<1)	_delay_ms(k*1000);
		k--;
	}
}

void waitForIDs()
{
	int mov;
	for (int k=1; k<=2; k++)
	{
		mov = dxl_read_word(k, P_MOVING);
		while (mov == 1)
		{
			mov = dxl_read_word(k, P_MOVING);
		}
	}
}


// Print communication result
void PrintCommStatus(int CommStatus)
{
	switch(CommStatus)
	{
	case COMM_TXFAIL:
		printf("COMM_TXFAIL: Failed transmit instruction packet!\n");
		break;

	case COMM_TXERROR:
		printf("COMM_TXERROR: Incorrect instruction packet!\n");
		break;

	case COMM_RXFAIL:
		printf("COMM_RXFAIL: Failed get status packet from device!\n");
		break;

	case COMM_RXWAITING:
		printf("COMM_RXWAITING: Now recieving status packet!\n");
		break;

	case COMM_RXTIMEOUT:
		printf("COMM_RXTIMEOUT: There is no status packet!\n");
		break;

	case COMM_RXCORRUPT:
		printf("COMM_RXCORRUPT: Incorrect status packet!\n");
		break;

	default:
		printf("This is unknown error code!\n");
		break;
	}
}


// Print error bit of status packet
void PrintErrorCode()
{
	if(dxl_get_rxpacket_error(ERRBIT_VOLTAGE) == 1)
		printf("Input voltage error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_ANGLE) == 1)
		printf("Angle limit error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_OVERHEAT) == 1)
		printf("Overheat error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_RANGE) == 1)
		printf("Out of range error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_CHECKSUM) == 1)
		printf("Checksum error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_OVERLOAD) == 1)
		printf("Overload error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_INSTRUCTION) == 1)
		printf("Instruction code error!\n");
}
